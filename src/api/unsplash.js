import axios from "axios";

export default axios.create({
  baseURL: "https://api.unsplash.com/",
  headers: {
    Authorization:
      "Client-ID 85674907587d458b106f1adabe90b585653e631ad5f0e158d3626310edcfa440"
  }
});
