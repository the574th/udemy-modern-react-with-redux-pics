import React from "react";

class SearchBar extends React.Component {
  state = { term: "" };

  // onFormSubmit(e) {
  //     e.preventDefault();
  //     console.log(this.state.term)
  // }

  //arrow functions automatically binds the value
  onFormSubmit = e => {
    e.preventDefault();

    this.props.onSubmit(this.state.term);
  };

  render() {
    return (
      <div className="ui segment">
        <form className="ui form" onSubmit={this.onFormSubmit}>
          <div className="field">
            <label>Image Search</label>
            <input
              type="text"
              value={this.state.term}
              onChange={e => this.setState({ term: e.target.value })}
            />
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBar;

// class SearchBar extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             term: 'yo gina',
//             firstTime: true
//         }
//         this.onClickChange = this.onClickChange.bind(this);
//     }
//     // state = { term: 'yo gina '};

//     onClickChange() {
//         this.state.firstTime ? (this.setState({ term: '', firstTime: false})) :
//         console.log(this.state.firstTime)
//     }

//     render() {
//         return (
//             <div className="ui segment">
//                 <form className="ui form">
//                     <div className="field">
//                         <label>Image Search</label>
//                         <input
//                             type="text"
//                             value={this.state.term}
//                             onClick={this.onClickChange}
//                             onChange={ e => this.setState({ term: e.target.value }) }
//                         />
//                     </div>
//                 </form>
//             </div>
//         )
//     }
// }

// export default SearchBar;